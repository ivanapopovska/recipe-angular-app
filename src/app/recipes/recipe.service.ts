import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppinglistService } from '../shoppinglist/shoppinglist.service';
import { Recipe } from './recipe.model';

@Injectable()
export class RecipeService{

    recipeSelected = new EventEmitter<Recipe>();

    constructor(private slService: ShoppinglistService){

    }
    recipes: Recipe[] = [
        new Recipe('Prv','Desc for prv','https://cleananddelicious.com/wp-content/uploads/2016/03/Avocad0-CD.jpg',
        [
          new Ingredient ("eggs", 2),
          new Ingredient("oats", 100)
        ]
        ),
        new Recipe('Vtor','Desc for prv','https://cleananddelicious.com/wp-content/uploads/2016/03/Avocad0-CD.jpg',
        [
          new Ingredient ("apples", 2),
          new Ingredient("milk", 100)
        ]
        ),
        new Recipe('Tret','Desc for prv','https://cleananddelicious.com/wp-content/uploads/2016/03/Avocad0-CD.jpg',
        [
          new Ingredient ("tomatoes", 2),
          new Ingredient("milk", 100)
        ]
        )
      ];

      addIngredientsToShoppinglist(ingredients: Ingredient[]){
          this.slService.addIngredients(ingredients);
      }
      

}